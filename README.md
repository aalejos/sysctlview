sysctlview 1.5.1
================

**sysctlview** is a sysctl MIB explorer for [FreeBSD](http://www.freebsd.org), 
it depends on [gtkmm](https://www.gtkmm.org), 
[sysctlmibinfo](https://gitlab.com/alfix/sysctlmibinfo) and
[sysctlinfo](https://gitlab.com/alfix/sysctlinfo); 
port: [deskutils/sysctlview](https://www.freshports.org/deskutils/sysctlview/).


Screenshots
-----------

Menu:  

![desktopMenu](screenshots/1_img.png)

Main Window:  

![values](screenshots/2_img.png)

OID Window:   

![description](screenshots/3_img.png)

Flags Window:   

![info](screenshots/4_img.png)

If the sysctlinfo module is loaded:  

Show CTL_MAXNAME levels:  

![CTL_MAXNAME](screenshots/5_img.png)

Display a subtree of CTLTYPE_NODEs:  

![values](screenshots/6_img.png)

Get the value of a node without the last name:   

![description](screenshots/7_img.png)

