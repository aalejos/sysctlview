# Any copyright is dedicated to the Public Domain.
# <http://creativecommons.org/publicdomain/zero/1.0/>
#
# Written by Alfonso S. Siciliano https://alfix.gitlab.io

CXXFLAGS += -std=c++11 `pkg-config --cflags gtkmm-3.0` -I/usr/local/include
LDFLAGS += -std=c++11 `pkg-config --libs gtkmm-3.0` -L/usr/local/lib -lsysctlmibinfo -lutil
OUTPUT = sysctlview 
SOURCES = main.cc mainwindow.cc trees.cc oidwindow.cc model.cc model_sysctlinfo.cc progressbarwindow.cc
OBJECTS = ${SOURCES:.cc=.o}
.PATH : ./src

all : ${OUTPUT}

clean:
	rm -f ${OUTPUT} *.o *~ *.core

${OUTPUT}: ${OBJECTS}
	${CXX} ${LDFLAGS} ${OBJECTS} -o ${.PREFIX}

.cc.o:
	${CXX} ${CXXFLAGS} -c ${.IMPSRC} -o ${.TARGET}
