/*
 * Copyright (c) 2019 Alfonso Sabato Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

#include <gtkmm/checkmenuitem.h>
#include <gtkmm/searchbar.h>
#include <gtkmm/searchentry.h>
#include <gtkmm/window.h>

#include "model.h"
#include "trees.h"

#define PROGRAM_NAME "sysctlview"
#define PROGRAM_VERSION "1.5.1"
#define COPYRIGHTSTRING "Copyright (c) 2019 Alfonso S. Siciliano"
#define LICENSE_PATH "/usr/local/share/licenses/sysctlview-" PROGRAM_VERSION "/BSD2CLAUSE"
#define ICON_PORT_PATH "/usr/local/share/icons/hicolor/scalable/apps/sysctlview.svg"
#define ICON_CWD_PATH "./icon/sysctlview.svg"

class MainWindow : public Gtk::Window
{

public:
    MainWindow();
    virtual ~MainWindow();

private:  	
    // members
    bool m_isIconSet;
    Trees *m_trees;
    Model *m_model;
    Gtk::SearchBar *m_searchBar;
    Gtk::CheckMenuItem *m_listCheckMain[NUM_COLSMAIN];
    Gtk::CheckMenuItem *m_listCheckFlags[NUM_COLSFLAGS];
    
    //Signal handlers (menu items)
    void on_refreshMenuItem();
    void on_rebuildMenuItem();
    void on_quitMenuItem();
    
    void on_sysctlMenuItem();
    void on_searchBarMenuItem();
    void on_wrapMenuItem();
    void on_expandsMenuItem();
    void on_collapseMenuItem();
    void on_allCheckMain();
    void on_defaultsCheckMain();
    void on_viewMainTreeColumn(int col);
    void on_allCheckFlags();
    void on_defaultsCheckFlags();
    void on_viewFlagsTreeColumn(int col);
    
    void on_flagsMenuItem();
    void on_faqMenuItem();
    void on_aboutMenuItem();

};

#endif // _MAINWINDOW_H_
