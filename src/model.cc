/*
 * Copyright (c) 2019 Alfonso Sabato Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <dev/evdev/input.h>  //struct input_id
#include <sys/resource.h>     //struct loadavg
#if defined(__amd64__) || defined(__i386__)
#include <machine/pc/bios.h>
#endif

#include <libutil.h>

#include <gtkmm/main.h>
#include <gtkmm/treeviewcolumn.h>

#include "model.h"
#include "progressbarwindow.h"
#include "model_sysctlinfo.h"

static const char *ctl_typename[CTLTYPE+1] = { 
    "ZEROUNUSED", 
    "node", 
    "integer", 
    "string", 
    "int64_t", 
    "opaque", 
    "unsigned integer", 
    "long integer", 
    "unsigned long", 
    "uint64_t", 
    "uint8_t", 
    "uint16_t", 
    "int8_t",
    "int16_t", 
    "int32_t", 
    "uint32_t" 
};

/* Public Methods */

Model::Model()
{
    int i;
    
    m_sysctlinfokmod = kld_isloaded("sysctlinfo") == 0 ? false : true;
    
    m_Columns = new Gtk::TreeModel::ColumnRecord();
    
    m_Columns->add(m_col_isMagicalRow);    
    m_Columns->add(m_col_name);
	    
    for(i=0;i<NUM_COLSMAIN; i++)
	m_Columns->add(*(mainTreeCols[i].modelColumn));
	    
    m_Columns->add(m_col_completename);
    m_Columns->add(m_col_uintflags);

    for(i=0;i<NUM_COLSFLAGS; i++)
	m_Columns->add(*(flagTreeCols[i].modelColumn));
	          
    //Create the Tree model
    m_refTreeModel = Gtk::TreeStore::create(*m_Columns);
    //Fill the TreeView's model
    populateTreeModel();

    m_flagShowMagicalRow = false;
    //Put the TreeModel inside a filter model:
    m_refTreeModelFilter = Gtk::TreeModelFilter::create(m_refTreeModel);
    m_refTreeModelFilter->set_visible_func(
	[this] (const Gtk::TreeModel::const_iterator& iter) -> bool
	{
	    //if(!iter)
	    //return true;

	    Gtk::TreeModel::Row row = *iter;
	    return (m_flagShowMagicalRow || !row[m_col_isMagicalRow]);
	});
}

Model::~Model()
{
}

Glib::RefPtr<Gtk::TreeModelFilter> Model::getRefTreeModelFilter()
{
    return m_refTreeModelFilter;
} 

void Model::refreshValues()
{
    m_countValues=0;
    ProgressBarWindow *pbw = new ProgressBarWindow("Refreshing Values",true);
    while(Gtk::Main::events_pending())
	    Gtk::Main::iteration();
	    
    m_refTreeModel->foreach_iter(sigc::mem_fun(*this,&Model::refreshValueIter));
    
    pbw->hide();
}

void Model::rebuildTrees()
{
    m_refTreeModel->clear();
    populateTreeModel();
    m_refTreeModelFilter->refilter();
}

void Model::toggleMagicalRows()
{
    m_flagShowMagicalRow = !m_flagShowMagicalRow;
    m_refTreeModelFilter->refilter();
}

Glib::ustring Model::getSysctlValue(Glib::ustring name, int longThreshold, Glib::ustring longstr)
{
    int id[SYSCTLMIF_MAXIDLEVEL];
    size_t idlevel = SYSCTLMIF_MAXIDLEVEL, namelen = name.size();
    Glib::ustring value("");
    struct sysctlmif_object *obj;
    
    if(m_sysctlinfokmod) {
    	obj = SysctlinfoHelper::buildSysctlmifObjectByName(name.c_str());
    }
    else {
    	sysctlmif_nametoid(name.c_str(), namelen, id, &idlevel);
    	obj = sysctlmif_object(id, idlevel, SYSCTLMIF_FALL);
    }
    value += getSysctlValue(obj, longThreshold, longstr);
    sysctlmif_freeobject(obj);
    
    return value;
}

bool Model::isSysctlinfokmodLoaded()
{
    return m_sysctlinfokmod;
}

/* Private Methods */

bool Model::refreshValueIter(const Gtk::TreeModel::iterator& iter)
{
    if(iter)
    {
	Gtk::TreeModel::Row row = *iter;
	m_countValues++;
	if(m_countValues > 100) {
		m_countValues=0;
    		while(Gtk::Main::events_pending())
	    		Gtk::Main::iteration();
	}
	row[m_col_value] = getSysctlValue(row[m_col_completename], 100, 
					  Glib::ustring("[CLICK TO SHOW]"));
    }
    
    return false;
}

void Model::preorderAddNode(struct sysctlmif_object *node, Gtk::TreeModel::Row *parent)
{
    int i;
    char *name;
    Glib::ustring usValue(""), idstr("");
    void *value;
    size_t valuesize=0;

    Gtk::TreeModel::Row row = parent != NULL
	? *(m_refTreeModel->append((*parent).children()))
	: *(m_refTreeModel->append());
    
    row[m_col_isMagicalRow] = node->id[0] == 0;
    
    for(i=0; i< node->idlevel; i++) {
	if(i>0)
	    idstr += ". ";
	idstr += Glib::ustring::compose("%1", node->id[i]);
    }
    row[m_col_id] = idstr;
	
    row[m_col_completename] = Glib::ustring(node->name);
    
    for (i=strlen(node->name); i >= 0 ;i--)
	if(node->name[i] == '.')
	    break;
    name = &(node->name[i+1]);
    row[m_col_name] = Glib::ustring(name);
    
    if(node->desc != NULL)
	row[m_col_desc] = Glib::ustring(node->desc);

    if(node->label != NULL)
	row[m_col_label] = Glib::ustring(node->label);

    if(node->fmt != NULL)
	row[m_col_fmt] = Glib::ustring(node->fmt);

    row[m_col_type] = Glib::ustring(ctl_typename[node->type]);

    row[m_col_flags] = Glib::ustring::compose("0x%1",
					      Glib::ustring::format(std::hex, node->flags));

    row[m_col_uintflags] = node->flags;

    // flags (bools)
    for(i=0; i < NUM_COLSFLAGS; i++)
	row[*(flagTreeCols[i].modelColumn)] = 
	    (node->flags & flagTreeCols[i].flag_bit) ==  flagTreeCols[i].flag_bit ? 1 : 0;

    // value
    row[m_col_value] = getSysctlValue(node, 100, Glib::ustring("[CLICK TO SHOW]"));

    struct sysctlmif_object *child;
    if(node->children !=NULL)
	if(!SLIST_EMPTY(node->children))
	    SLIST_FOREACH(child, node->children, object_link)
		preorderAddNode(child,&row);
}

void Model::populateTreeModel()
{
    struct sysctlmif_object *root, *nodelev1;
    struct sysctlmif_object_list *toplist;
    int id[1] = {0};
    size_t idlevel = 0;

    ProgressBarWindow *pbw = new ProgressBarWindow("Building Tree",false);
    while(Gtk::Main::events_pending())
	    Gtk::Main::iteration();
    
    if(m_sysctlinfokmod)
    	root = SysctlinfoHelper::buildSysctlmifTree();
    else
    	root = sysctlmif_tree(id, idlevel, SYSCTLMIF_FALL, SYSCTLMIF_MAXDEPTH);
    
    toplist = root->children;

    while (!SLIST_EMPTY(toplist))
    {
	nodelev1 = SLIST_FIRST(toplist);
	pbw->update(nodelev1->name);
    	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
	preorderAddNode(nodelev1, NULL);
	SLIST_REMOVE_HEAD(toplist, object_link);
	sysctlmif_freetree(nodelev1);
    }
    sysctlmif_freeobject(root);

    pbw->hide();
}

Glib::ustring Model::getSysctlValue(struct sysctlmif_object * object, 
				    int longThreshold, Glib::ustring longstr)
{
    size_t valuesize = 0;
    void *value;
    Glib::ustring usValue("");
    int i, power10;
    float base;
    
    if(object->type == CTLTYPE_NODE)
	return usValue;
    
    if(object->type == CTLTYPE_OPAQUE)
	if(strcmp(object->fmt, "S,clockinfo") != 0 && 
	   strcmp(object->fmt, "S,input_id") != 0 &&
	   strcmp(object->fmt, "S,loadavg") != 0 &&
#if defined(__amd64__) || defined(__i386__)
	   strcmp(object->fmt, "S,bios_smap_xattr") != 0 &&
#endif
	   strcmp(object->fmt, "S,timeval") != 0)
	    return usValue;
    
    if(sysctl(object->id, object->idlevel, NULL, &valuesize, NULL, 0) !=0)
	return usValue;

    if(valuesize > longThreshold && longThreshold > 0) {
	usValue += longstr;
	return usValue;
    }
   
    /* valuesize could change after 2 calls of sysctl() */
    valuesize += valuesize;
    value = malloc(valuesize);
    memset(value, 0, valuesize);
    
    if(sysctl(object->id, object->idlevel, value, &valuesize, NULL, 0) != 0)
    {
	// usValue = "";
    }
    else if(object->type == CTLTYPE_OPAQUE)
    {
	if(strcmp(object->fmt, "S,clockinfo") == 0)
	{
	    struct clockinfo *ci = (struct clockinfo*)value;
	    usValue += Glib::ustring::compose("{ hz = %1, tick = %2, "\
					      "profhz = %3, stathz = %4 }", 
					      ci->hz, ci->tick, 
					      ci->profhz, ci->stathz);
	}
#if defined(__amd64__) || defined(__i386__)
	else if(strcmp(object->fmt, "S,bios_smap_xattr") == 0)
	{
	    struct bios_smap_xattr *smap, *end;

	    end = (struct bios_smap_xattr *)((char *)value + valuesize);
	    for (smap = (struct bios_smap_xattr *)value; smap < end; smap++) {
		usValue += Glib::ustring::compose("\nSMAP type=%1, xattr=%2, "\
						  "base=%3, len=%4",
						  Glib::ustring::format(std::hex, smap->type),
						  Glib::ustring::format(std::hex, smap->xattr),
						  Glib::ustring::format(std::hex, (uintmax_t)smap->base),
						  Glib::ustring::format(std::hex, (uintmax_t)smap->length));
	    }
	}
#endif
	else if(strcmp(object->fmt, "S,input_id") == 0)
	{
	    struct input_id *id = (struct input_id*)value;
	    usValue += Glib::ustring::compose("{ bustype = 0x%1, vendor = 0x%2, "\
					      "product = 0x%3, version = 0x%4 }",
					      Glib::ustring::format(std::hex, id->bustype), 
					      Glib::ustring::format(std::hex, id->vendor), 
					      Glib::ustring::format(std::hex, id->product), 
					      Glib::ustring::format(std::hex, id->version));
	}
	else if(strcmp(object->fmt, "S,loadavg") == 0)
	{
	    struct loadavg *tv = (struct loadavg*)value;
	    usValue += Glib::ustring::compose("{ %1 %2 %3 }",
					      Glib::ustring::format(std::hex, (double)tv->ldavg[0]/(double)tv->fscale),
					      Glib::ustring::format(std::hex, (double)tv->ldavg[1]/(double)tv->fscale),
					      Glib::ustring::format(std::hex, (double)tv->ldavg[2]/(double)tv->fscale));
	}
	else if(strcmp(object->fmt, "S,timeval") == 0)
	{
	    struct timeval *tv = (struct timeval*)value;
	    time_t tv_sec;
	    char *p1;
	    usValue += Glib::ustring::compose("{ sec = %1, usec = %2 } ",
					      (intmax_t)tv->tv_sec, tv->tv_usec);
	    tv_sec = tv->tv_sec;
	    p1 = strdup(ctime(&tv_sec));
	    p1[strlen(p1) -1] = '\0';
	    usValue += " ";
	    usValue += p1;
	    free(p1);
	}
    }
    else if(object->type == CTLTYPE_STRING)
    {
    	char *tmpstr = (char*)value;
	if (tmpstr[valuesize] != '\0')
	    tmpstr[valuesize] = '\0';
	if (tmpstr[strlen(tmpstr)-1] == '\n')
            tmpstr[strlen(tmpstr)-1] = '\0';
	usValue += Glib::ustring((char*)value);
    }
#define GTVL(typevar) do {						\
        for (i=0; i < valuesize / sizeof( typevar); i++) {		\
            if (i > 0)							\
                usValue += " ";						\
            usValue += Glib::ustring::compose("%1", ((typevar *)value)[i]); \
        }								\
    } while(0)
    else if(object->type == CTLTYPE_INT)
	if (object->fmt[1] == 'K') {
	    power10 = 1;
	    if (object->fmt[2] != '\0')
		power10 = object->fmt[2] - '0';
	    base = 1.0;
	    for (i = 0; i < power10; i++)
		base *= 10.0;
	    usValue += Glib::ustring::compose("%1 C", *((int*)value) / base - 273.15);
	} else {
	    GTVL(int);
	}
    else if(object->type == CTLTYPE_S8)
	GTVL(int8_t);
    else if(object->type == CTLTYPE_S16)
	GTVL(int16_t);
    else if(object->type == CTLTYPE_S32)
	GTVL(int32_t);
    else if(object->type == CTLTYPE_LONG)
	GTVL(long);
    else if(object->type == CTLTYPE_S64)
	GTVL(int64_t);
    else if(object->type == CTLTYPE_UINT)
	GTVL(u_int);
    else if(object->type == CTLTYPE_U8)
	GTVL(uint8_t);
    else if(object->type == CTLTYPE_U16)
	GTVL(uint16_t);
    else if(object->type == CTLTYPE_U32)
	GTVL(uint32_t);
    else if(object->type == CTLTYPE_ULONG)
	GTVL(unsigned long);
    else if(object->type == CTLTYPE_U64)
	GTVL(uint64_t);
    else // opaque
	usValue += "";
    
    free(value);
    
    return usValue;
}
