/*
 * Copyright (c) 2019 Alfonso Sabato Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>
#include <stdlib.h>

#include "model_sysctlinfo.h"

/* Util */

static struct sysctlmif_object *
allocSysctlmibObject(int *id, size_t idlevel, char* namep, char* descp, u_int kind, char* fmtp, char* labelp)
{
    struct sysctlmif_object *object;

    if((object = (struct sysctlmif_object *)malloc(sizeof(struct sysctlmif_object))) == NULL)
    	return NULL;
    memset(object, 0, sizeof(struct sysctlmif_object));
    
    object->idlevel = idlevel;
    if((object->id = (int*)malloc(sizeof(int) * idlevel)) == NULL)
    	return NULL;
    memcpy(object->id, id, sizeof(int) * idlevel);
    object->type =  kind & CTLTYPE;
    object->flags = kind & 0xfffffff0;
    object->name = strdup(namep);
    object->desc = strdup(descp);
    object->fmt = strdup(fmtp);
    object->label = strdup(labelp);
    
    return object;
}

static struct sysctlmif_object *
buildTree(int id[CTL_MAXNAME], size_t idlevel, int idnext[CTL_MAXNAME], size_t *idnextlevel)
{
    struct sysctlmif_object *node, *last, *child;
    struct sysctlmif_object_list *list = NULL;
    int *idp_unused, error, i, prop[2], *idnextp;
    size_t buflen, idlevel_unused, idnextlevelp;
    char *buf, *namep, *descp, *fmtp, *labelp;
    unsigned int kind;
    
    prop[0] = CTL_SYSCTLMIB;
    prop[1] = ENTRYALLINFO_WITHNEXTNODE;
    if(SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0)
    	return NULL;
    if((buf = (char*)malloc(buflen)) == NULL)
    	return NULL;
    if(SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
    	return NULL;
    SYSCTLINFO_HELPER_ALLWITHNEXT(buf, idlevel_unused, idp_unused, namep, descp,
    					kind, fmtp, labelp, idnextlevelp, idnextp);
    
    node = allocSysctlmibObject(id, idlevel, namep, descp, kind, fmtp, labelp);   
    if( (list = (struct sysctlmif_object_list *)malloc(sizeof(struct sysctlmif_object_list))) == NULL){
	return (NULL);
    }
    node->children = list;
    SLIST_INIT(list);
    
    *idnextlevel = idnextlevelp;
    memcpy(idnext, idnextp, *idnextlevel * sizeof(int));

    int childid[CTL_MAXNAME];
    size_t childidlevel;
    while(*idnextlevel > idlevel) {
        memcpy(childid, idnext, *idnextlevel * sizeof(int));
    	childidlevel = *idnextlevel;
    	if((child = buildTree(childid, childidlevel, idnext, idnextlevel)) == NULL)
    		break;
    	if (SLIST_EMPTY(list)) {
		SLIST_INSERT_HEAD(list, child, object_link);
    	} else {
		SLIST_INSERT_AFTER(last, child, object_link);
    	}
    	last = child;
    }
    
    free(buf);
    
    return node;
}

/* API */

struct sysctlmif_object *SysctlinfoHelper::buildSysctlmifObjectByName(const char *name)
{
    struct sysctlmif_object *object = NULL;
    char *namep, *descp, *fmtp, *labelp, *buf = NULL;
    size_t buflen = 0, idlevel;
    int prop[2] = {CTL_SYSCTLMIB, ENTRYALLINFOBYNAME}, *id;
    unsigned int kind;
    
    
    if(SYSCTLINFO_BYNAME(name, prop, NULL, &buflen) != 0)
    	return NULL;
    if((buf = (char*)malloc(buflen)) == NULL)
    	return NULL;
    if(SYSCTLINFO_BYNAME(name, prop, buf, &buflen) != 0)
    	return NULL;
    SYSCTLINFO_HELPER_ALL(buf, idlevel, id, namep, descp, kind, fmtp, labelp);
    
    object = allocSysctlmibObject(id, idlevel, namep, descp, kind, fmtp, labelp);
    
    free(buf);
    
    return object;
}

struct sysctlmif_object *SysctlinfoHelper::buildSysctlmifTree()
{
    struct sysctlmif_object *root, *last, *child;
    struct sysctlmif_object_list *list = NULL;
    int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
    size_t idlevel = 0, idnextlevel;
    
    if((root = (struct sysctlmif_object *)malloc(sizeof(struct sysctlmif_object))) == NULL)
    	return NULL;
    memset(root, 0, sizeof(struct sysctlmif_object));
    root->id = (int*)malloc(sizeof(int));
    root->id[0] = 0;
    if( (list = (struct sysctlmif_object_list *)malloc(sizeof(struct sysctlmif_object_list))) == NULL)
	return NULL;
    root->children = list;
    SLIST_INIT(list);

    id[0] = 0;
    idlevel = 1;
    while(true) {
    	if ( (child = buildTree(id, idlevel, idnext, &idnextlevel)) == NULL)
    		return NULL;
    	
    	if (SLIST_EMPTY(list)) {
		SLIST_INSERT_HEAD(list, child, object_link);
    	} else {
		SLIST_INSERT_AFTER(last, child, object_link);
    	}
    	last = child;
    	
    	if(idnextlevel == 0)
    		break;
    	memcpy(id, idnext, idnextlevel * sizeof(int));
    	idlevel = idnextlevel;
    }

    return root;
}

