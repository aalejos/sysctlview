/*
 * Copyright (c) 2019 Alfonso Sabato Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <gtkmm/adjustment.h>
#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/textbuffer.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/viewport.h>

#include "oidwindow.h"

OIDWindow::OIDWindow(Gtk::TreeModel::Row row, Model *model)
{   
    int i;
    bool isFirst=true;
    Glib::ustring prop(""), value(""), flagsValue("");
    Glib::RefPtr<Gtk::Adjustment> h = Gtk::Adjustment::create(0,0,100);
    Glib::RefPtr<Gtk::Adjustment> w = Gtk::Adjustment::create(0,0,100);
    Gtk::Viewport *viewport = new Gtk::Viewport(h,w);
    Gtk::Grid *grid = Gtk::manage( new Gtk::Grid() );
    Gtk::Label *tmpLabel;
    Glib::RefPtr<Gtk::TextBuffer> tmpBuffer;
    Gtk::TextView *tmpTextView;
    Gtk::ScrolledWindow *scrolledWindow = Gtk::manage(new Gtk::ScrolledWindow());
    
    struct field {
	const char *name;
	Glib::ustring value;
    };

    // Set this Window
    set_default_size(500, 400);
    set_resizable(true);
    set_title(row[model->m_col_name]);
    set_border_width(10);
    
    // build the widgets
    grid->set_row_spacing(5);
    grid->set_column_spacing(8);
    
    for(i=0; i<NUM_COLSFLAGS; i++) {
	if(row[*(model->flagTreeCols[i].modelColumn)]) {
	    if(!isFirst)
	     flagsValue += "\n";
	    isFirst = false;
	    flagsValue += model->flagTreeCols[i].flag_name;
	    flagsValue += ",  "; 
	    flagsValue += model->flagTreeCols[i].flag_desc;
	}
    }
    
    value = model->getSysctlValue(row[model->m_col_completename], -1, "ERROR");
    if(value.size() <= 0){
	value = "";
    }
    
    struct field fields[8] = {
	{"ID", row[model->m_col_id] },
	{"Name", row[model->m_col_completename] },
	{"Description", row[model->m_col_desc] },
	{"Label", row[model->m_col_label] },
	{"Type", row[model->m_col_type] },
	{"Format", row[model->m_col_fmt] },
	{"Flags", flagsValue },
	{"Value", value }
    };
    

    for(i=0; i< 8; i++) {
	tmpLabel = Gtk::manage( new Gtk::Label(fields[i].name) );
	tmpLabel->set_alignment(1,0);
	grid->attach(*tmpLabel, 0, i, 1, 1);
	
	tmpBuffer = Gtk::TextBuffer::create();
	tmpBuffer->set_text(fields[i].value);
	tmpTextView = Gtk::manage( new Gtk::TextView());
	tmpTextView->set_buffer(tmpBuffer);
	tmpTextView->set_hexpand(true);
	tmpTextView->set_left_margin(3);
	tmpTextView->set_right_margin(3);
	//if(i != 7) // value
	    tmpTextView->set_wrap_mode(Gtk::WRAP_WORD_CHAR);
	tmpTextView->set_editable(false);
	tmpTextView->set_cursor_visible(false);
	grid->attach(*tmpTextView, 1, i, 1, 1);
    }
          
    
    scrolledWindow->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    scrolledWindow->set_propagate_natural_width(true);
    scrolledWindow->set_propagate_natural_height(true);
    viewport->add(*grid);
    scrolledWindow->add(*viewport);
    add(*scrolledWindow);
    show_all_children();
    show();

}
