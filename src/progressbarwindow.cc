/*
 * Copyright (c) 2019 Alfonso Sabato Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "progressbarwindow.h"

ProgressBarWindow::ProgressBarWindow(Glib::ustring nameWindow, bool automode)
{    
   
    set_title(nameWindow);
    set_default_size(300,150);
    set_border_width(3);
    set_deletable(false);
    set_resizable(false);
    set_modal(true);
    
    m_progressBar = Gtk::manage( new Gtk::ProgressBar() );
    m_progressBar->set_fraction(0.0);
    m_progressBar->set_halign(Gtk::ALIGN_CENTER);
    m_progressBar->set_valign(Gtk::ALIGN_CENTER);
    m_progressBar->set_text("Loading...");
    m_progressBar->set_show_text(true);
    
    add(*m_progressBar);

    show_all_children();
    show();
    
    //Add a timer callback to update the value of the progress bar:
    if(automode)
          m_connection_timeout = Glib::signal_timeout().connect(sigc::mem_fun(*this,
                                              &ProgressBarWindow::on_automode), 50 );
}

ProgressBarWindow::~ProgressBarWindow()
{
}

void ProgressBarWindow::update(Glib::ustring name)
{
     m_progressBar->set_text("\'" + name + "\' subtree");
     m_progressBar->pulse();
}

bool ProgressBarWindow::on_automode()
{
    m_progressBar->pulse();

  //As this is a timeout function, return true so that it
  //continues to get called
  return true;
}

