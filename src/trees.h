/*
 * Copyright (c) 2019 Alfonso Sabato Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _MYTREEVIEW_H_
#define _MYTREEVIEW_H_

#include <gtkmm/window.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/searchentry.h>
#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>

#include "model.h"

#define WRAP_SIZE_NAME_COL 150
#define WRAP_SIZE_COLS 300

class Trees
{

public:
    Trees(Gtk::Window *w, Gtk::SearchEntry *searchEntry, Model *m);
    virtual ~Trees();

    Gtk::TreeView* getMainTree();
    Gtk::TreeView* getFlagsTree();
    void expandTrees();
    void collapseTrees();
    void toggleWrapRows();
    void toggleMainTreeColumn(int col);
    void toggleFlagsTreeColumn(int col);
    void autoResizeColumns();

private:

    // members:
    Gtk::TreeView *m_mainTree;
    Gtk::TreeView *m_flagsTree;
    Gtk::TreeViewColumn *m_skipColumn;
    Gtk::TreeViewColumn *m_dormantColumn;
    Gtk::Window *m_Window; // to show dialog
    Model *m_model;
    
    // methods:
    void setColumnColorIfBlack(Gtk::TreeViewColumn *tvc, Glib::ustring color);
    bool on_search_equal(const Glib::RefPtr<Gtk::TreeModel>& model, int column, 
			 const Glib::ustring& key, const Gtk::TreeModel::iterator& iter);
    
    // signal handlers:
    void on_rowActivated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
   
};

#endif // _MYTREEVIEW_H_
