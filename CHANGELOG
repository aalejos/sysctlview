2019-09-12 version 1.5.1

	* fix "View -> Magical nodes" when the sysctlinfo module is loaded

2019-09-07 version 1.5

	* add 'sysctlinfo' kernel interface support:
	* add a warning dialog if the sysctlinfo kmod is not loaded
	* handle nodes up to CTL_MAXNAME levels
	* show the right value of the nodes without the last name
	* show subtrees without leaves (all the node are CTLTYPE_NODE)
	* improve efficiency: sysctlinfo is 30% more efficient than the kernel undocumented interface

2019-06-25 version 1.4

	* improve searching
	* change dafault 'Flags' columns
	* change searchEntry size
	* change "[VALUE TOO LONG, CLICK TO SHOW]" -> "[CLICK TO SHOW]"
	* add menu item "View -> Main tree -> All"
	* add menu item "View -> Main tree -> Defaults"
	* add menu item "View -> Flags tree -> All"
	* add menu item "View -> Flags tree -> Defaults"
	* add support for S,bios_smap_xattr opaque value (e.g., machdep.smap)

2019-06-11 version 1.3

	* fix memory leak OIDWindow
	* fix columns size after "Rebuild all"
	* fix AND flags
	* change OIDWindow layout
	* add ProgressBar (progressbar.h/cc)
	* add 'ID' column
	* add menu item "View -> Search Name"
	* add menu item "View -> Wrap Rows"


2019-05-28 version 1.2

	* fix Kelvin ("IK*" format) value
	* fix length of a column (set wrap property)
	* change RowWindow -> OIDWindow
	* add Model class (model.h/cc)
	* add menu item "File -> refresh values"
	* add menu item "File -> rebuild the tree"
	* add menu "View"
	* add menu item "View -> exapand all nodes"
	* add menu item "View -> collapse the tree"
	* add menu item "View -> show magical rows"
	* add menu items "View -> Main tree -> 'info' column"
	* add menu items "View -> Flags tree -> 'flag' column"
	* add menu item "Help -> FAQ"
	* add support for S,clockinfo opaque value (e.g., kern.clockrate)
	* add support for S,input_id opaque value (e.g., kern.evdev.input.0.id)
	* add support for S,loadavg opaque value (e.g., vm.loadavg)
	* add support for S,timeval opaque value (e.g., kern.boottime)


2019-05-22 version 1.1

	* add copyright to "aboutDialog"
	* add logo to "aboutDialog"
	* add icon to "MyWindow"
	* fix 'PR237159' build with GCC-based architectures
	* fix font color of the 'name' column depending on the theme
	* fix valuelen with  "double sysctl() call" (e.g., hw.dri.0.vblank)
	* delete footer label (copyright)


2019-03-29 version 1.0

	* add "LONG VALUE, CLICK TO SHOW" (e.g., kern.conftxt)
	* add icons
	* update "aboutDialog"
	* update show a Window when a row is clicked, no a Dialog
	* update 'resizable' columns in "Main" TreeView
	* update 'resizable' "name" column in "Flags" TreeView
	* fix numeric array (e.g., kern.cp_times)
	* fix don't get the value of a CTLTYPE_NODE
	* fix unicode values (e.g., kern.cam.ada.0)
	* fix clean code


2019-03-23 version 0.2

	* change C GTK -> C++ GTKmm
	* add menu
	* add "node dialog" when a row is clicked
	* add manual page "sysctlview.1"
	* add TreeView "Main"
	* delete TreeView "description"
	* delete TreeView "values"
	* delete TreeView "info"
	* delete sysctlmibinfo.c/h (dynamic linking)
	* delete sysctlview.window.xml


2019-02-02 version 0.1

	* add TreeView "description"
	* add TreeView "values"
	* add TreeView "info"
	* add TreeView "flags"
